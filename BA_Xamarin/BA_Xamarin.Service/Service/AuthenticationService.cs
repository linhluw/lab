﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.Infrastructure.Repository;
using BA_Xamarin.Entities.RealmEntity;
using BA_Xamarin.Utilities.Logs;

namespace BA_Xamarin.Service
{
    public class AuthenticationService : RealmBaseService<UserRealm, UserRespone>, IAuthenticationService
    {
        private readonly IRequestProvider _IRequestProvider;

        public AuthenticationService(IRequestProvider requestProvider, IBaseRepository baseRepository) : base(baseRepository)
        {
            _IRequestProvider = requestProvider;
        }

        public UserRespone LoginAsync(LoginRequest request)
        {
            UserRespone user = null;
            try
            {
                if (request.UserName == "linhlv" && request.Password == "123456")
                {
                    return new UserRespone()
                    {
                        Email = "linhlv@bagroup.vn",
                        FullName = "Lưu Văn Linh",
                        Password = request.Password,
                        UserName = request.UserName
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            return user;
        }
    }
}
