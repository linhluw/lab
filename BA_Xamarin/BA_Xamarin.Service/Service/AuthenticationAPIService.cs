﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.Infrastructure.Repository;
using BA_Xamarin.Entities.RealmEntity;
using BA_Xamarin.Utilities.Logs;

namespace BA_Xamarin.Service
{
    public class AuthenticationAPIService : RealmBaseService<UserRealm, UserRespone>, IAuthenticationAPIService
    {
        private readonly IRequestProvider _IRequestProvider;

        public AuthenticationAPIService(IRequestProvider requestProvider, IBaseRepository baseRepository) : base(baseRepository)
        {
            _IRequestProvider = requestProvider;
        }

        public async Task<List<UserRespone>> LoginAsync(LoginRequest request)
        {
            List<UserRespone> user = null;
            try
            {
                var result = await _IRequestProvider.PostAsync<LoginRequest, List<UserRespone>>("api/user", request, string.Empty, string.Empty);
                if (result != null)
                {
                    user = result;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            return user;
        }
    }
}
