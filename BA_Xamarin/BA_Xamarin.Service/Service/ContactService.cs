﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.Infrastructure.Repository;
using BA_Xamarin.Entities.RealmEntity;
using BA_Xamarin.Utilities.Logs;

namespace BA_Xamarin.Service
{
    public class ContactService : RealmBaseService<ContactRealm, ContactRespone>, IContactService
    {
        private readonly IRequestProvider _IRequestProvider;

        public ContactService(IRequestProvider requestProvider, IBaseRepository baseRepository) : base(baseRepository)
        {
            _IRequestProvider = requestProvider;
        }

        public ContactRespone SaveAsync(ContactRequest request)
        {
            ContactRespone contact = null;
            try
            {
                //var result = await _IRequestProvider.PostAsync<LoginRequest, LoginResponse>("Api của bạn", request);
                //if (result != null)
                //{
                //    user = result;
                //}

                return new ContactRespone()
                {
                    Id = request.Id != 0 ? request.Id : GetLastId() + 1,
                    Name = request.Name,
                    Position = request.Position,
                    Phone = request.Phone,
                    Age = request.Age,
                    Gender = request.Gender,
                    Email = request.Email,
                    Adress = request.Adress
                };

            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            return contact;
        }

        public ContactRespone DeleteAsync(ContactRequest request)
        {
            ContactRespone contact = null;
            try
            {
                //var result = await _IRequestProvider.PostAsync<LoginRequest, LoginResponse>("Api của bạn", request);
                //if (result != null)
                //{
                //    user = result;
                //}

                return new ContactRespone()
                {
                    Id = request.Id
                };
            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            return contact;
        }
    }
}
