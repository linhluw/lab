﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.Infrastructure.Repository;
using BA_Xamarin.Entities.RealmEntity;
using BA_Xamarin.Utilities.Logs;

namespace BA_Xamarin.Service
{
    public class ContactAPIService : RealmBaseService<ContactRealm, ContactRespone>, IContactAPIService
    {
        private readonly IRequestProvider _IRequestProvider;

        public ContactAPIService(IRequestProvider requestProvider, IBaseRepository baseRepository) : base(baseRepository)
        {
            _IRequestProvider = requestProvider;
        }

        public async Task<IEnumerable<ContactRespone>> GetAll()
        {
            IEnumerable<ContactRespone> contact = null;
            try
            {
                var result = await _IRequestProvider.GetAsync<IEnumerable<ContactRespone>>("api/contact", string.Empty);
                if (result != null)
                {
                    contact = result;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            return contact;
        }

        public async Task<string> SaveAsync(ContactRequest request)
        {
            string str = "Fail";
            try
            {
                if (request.Id != 0) //update 
                {
                    str = await _IRequestProvider.PutAsync<ContactRequest, string>("api/contact/" + request.Id, request, string.Empty, string.Empty);
                }
                else
                {
                    str = await _IRequestProvider.PostAsync<ContactRequest, string>("api/contact", request, string.Empty, string.Empty);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            return str;
        }


        public async Task<string> DeleteAsync(ContactRequest request)
        {
            string str = "Fail";
            try
            {
                if (request.Id != 0) //update 
                {
                    str = await _IRequestProvider.DeleteAsync<ContactRequest, string>("api/contact/" + request.Id, string.Empty);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            return str;
        }

    }
}
