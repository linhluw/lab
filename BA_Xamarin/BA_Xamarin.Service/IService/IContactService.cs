﻿using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.RealmEntity;

namespace BA_Xamarin.Service
{
    public interface IContactService : IRealmBaseService<ContactRealm, ContactRespone>
    {
        ContactRespone SaveAsync(ContactRequest request);

        ContactRespone DeleteAsync(ContactRequest request);
    }
}
