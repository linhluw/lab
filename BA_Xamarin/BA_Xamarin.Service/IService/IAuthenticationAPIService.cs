﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.RealmEntity;

namespace BA_Xamarin.Service
{
    public interface IAuthenticationAPIService : IRealmBaseService<UserRealm, UserRespone>
    {
        Task<List<UserRespone>> LoginAsync(LoginRequest request);
    }
}
