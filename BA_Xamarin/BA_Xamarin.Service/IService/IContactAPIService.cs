﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.RealmEntity;

namespace BA_Xamarin.Service
{
    public interface IContactAPIService : IRealmBaseService<ContactRealm, ContactRespone>
    {

        Task<IEnumerable<ContactRespone>> GetAll();

        Task<string> SaveAsync(ContactRequest request);

        Task<string> DeleteAsync(ContactRequest request);


    }
}
