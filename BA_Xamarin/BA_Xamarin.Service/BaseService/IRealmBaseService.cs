﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BA_Xamarin.Entities;
using Realms;

namespace BA_Xamarin.Service
{
    public interface IRealmBaseService<TEntity, TViewModel>
        where TEntity : RealmObject, IRealmEntity
        where TViewModel : class
    {
        TViewModel Add(TViewModel viewModel);
        void AddRange(IEnumerable<TViewModel> viewModels);

        void AddRangeAsync(IEnumerable<TViewModel> viewModels);

        TViewModel Update(TViewModel viewModel);
        void Delete(long id);

        bool Any();

        TViewModel Get(long id);
        TViewModel Get(Expression<Func<TEntity, bool>> predicate);
        long GetLastId();

        IEnumerable<TViewModel> All();
        IEnumerable<TViewModel> Find(Expression<Func<TEntity, bool>> predicate);
    }
}
