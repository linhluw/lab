﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using BA_Xamarin.Core.Droid;
using BA_Xamarin.Core.Droid.Setup;
using Xamarin.Forms.GoogleMaps.Android;

namespace BA_Xamarin.Droid
{
    [Activity(Label = "BA_Xamarin", Icon = "@mipmap/logo", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : BaseActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);


            BAXamarinSetup.Initialize(this, bundle);

            //var platformConfig = new PlatformConfig
            //{
            //    BitmapDescriptorFactory = new CachingNativeBitmapDescriptorFactory()
            //};

            //Xamarin.FormsGoogleMaps.Init(this, bundle, platformConfig); // initialize for Xamarin.Forms.GoogleMaps

            Xamarin.FormsGoogleMaps.Init(this, bundle);

            LoadApplication(new BA_XamarinApp(new AndroidInitializer()));
        }
    }
}

