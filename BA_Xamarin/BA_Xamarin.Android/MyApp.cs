﻿using Android.App;
using Android.Runtime;
using System;

namespace BA_Xamarin.Droid
{
    [Application]
    [MetaData("com.google.android.maps.v2.API_KEY",
              Value = "AIzaSyDJ1yDrTnpxGt8kKsqcXl_34PHZYM9otWE")]
    public class MyApp : Application
    {
        public MyApp(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }
    }
}