﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Prism;
using Prism.Ioc;
using UIKit;

namespace BA_Xamarin.Core.iOS
{
    public class BaseDelegate : Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public class IOSInitializer : IPlatformInitializer
        {
            public void RegisterTypes(IContainerRegistry containerRegistry)
            {
                // Register any platform specific implementations

            }
        }
    }
}