﻿using Xamarin.Forms.Platform.iOS;

namespace BA_Xamarin.Core.iOS.Setup
{
    public static class BAXamarinSetup
    {
        public static FormsApplicationDelegate AppDelegate;

        public static void Initialize(FormsApplicationDelegate _AppDelegate)
        {
            AppDelegate = _AppDelegate;

            //Đăng ký các packed ngoài ở đây
            //ví dụ 
            // XamEffects.iOS.Effects.Init(); //write here
        }
    }
}