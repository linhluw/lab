﻿using Realms;

namespace BA_Xamarin.Entities
{
	public interface IRealmEntity : IEntity
	{
		RealmInteger<int> Counter { get; set; }
	}
}
