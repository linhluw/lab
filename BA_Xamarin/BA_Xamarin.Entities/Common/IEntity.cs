﻿using System;

namespace BA_Xamarin.Entities
{
	public interface IEntity
	{
		long Id { get; set; }
		DateTimeOffset? LastSynchronizationDate { get; set; }
		DateTimeOffset? SysLastChangeDate { get; set; }
		DateTimeOffset? SysDeletedDate { get; set; }
	}
}
