﻿using Realms;

namespace BA_Xamarin.Entities.Infrastructure.Repository
{
	public interface IRealmConnection
	{
		Realm Connection { get; }
		void Destroy();
    }
}
