﻿namespace BA_Xamarin.Entities
{
    public class LoginRequest
    {

        public string UserName { set; get; }

        public string Password { set; get; }

    }
}
