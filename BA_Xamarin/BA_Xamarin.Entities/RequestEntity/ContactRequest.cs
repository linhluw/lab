﻿namespace BA_Xamarin.Entities
{
    public class ContactRequest
    {

        public long Id { get; set; }

        public string Name { get; set; }

        public string Position { get; set; }

        public string Phone { get; set; }

        public int Age { get; set; }

        public string Gender { get; set; }

        public string Email { get; set; }

        public string Adress { get; set; }

    }
}
