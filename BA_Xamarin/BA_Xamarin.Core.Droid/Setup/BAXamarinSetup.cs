﻿
using Android.App;
using Android.OS;

namespace BA_Xamarin.Core.Droid.Setup
{
    public static class BAXamarinSetup
    {
        public static Activity Activity;

        public static void Initialize(Activity activity, Bundle bundle)
        {
            Activity = activity;

            //Đăng ký các packed ngoài ở đây
            //ví dụ 
            // XamEffects.iOS.Effects.Init(); //write here
        }
    }
}