﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.Constant;
using BA_Xamarin.Service;
using BA_Xamarin.Utilities.Logs;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms;

namespace BA_Xamarin.Core.ViewModels
{
    public class MainDetailAPIViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;

        private readonly IContactAPIService _contactService;


        private long id;
        private string name;
        private string position;
        private string phone;
        private int age;
        private string gender;
        private string email;
        private string adress;

        public MainDetailAPIViewModel(INavigationService navigationService, IContactAPIService contactService) : base(navigationService)
        {
            this._navigationService = navigationService;
            this._contactService = contactService;
            OnSaveClicked = new DelegateCommand(ExecuteOnSaveClickedCommandAsync);
            OnDeleteClicked = new DelegateCommand(ExecuteOnDeleteClickedCommandAsync);
            OnCancelClicked = new DelegateCommand(ExecuteOnCancelClickedCommandAsync);
            Title = "Contact Details";
        }

        public long Id { get => id; set => SetProperty(ref id, value); }

        public string Name { get => name; set => SetProperty(ref name, value); }

        public string Position { get => position; set => SetProperty(ref position, value); }

        public string Phone
        {
            get => phone;
            set
            {
                if (SetProperty(ref phone, value))
                {
                    NumberChangedAsync();
                }
            }
        }

        public int Age { get => age; set => SetProperty(ref age, value); }

        private void NumberChangedAsync()
        {
            bool isValid = Phone.ToCharArray().All(x => char.IsDigit(x));
            Phone = isValid ? Phone : Phone.Remove(Phone.Length - 1);
        }
        public string Gender { get => gender; set => SetProperty(ref gender, value); }

        public string Email { get => email; set => SetProperty(ref email, value); }

        public string Adress { get => adress; set => SetProperty(ref adress, value); }

        private bool isshow;
        public bool IsShow
        {

            get => isshow;
            set
            {
                if (SetProperty(ref isshow, value))
                {
                    RaisePropertyChanged(nameof(IsNotShow));
                }
            }
        }

        public bool IsNotShow => !IsShow;
        public DelegateCommand OnSaveClicked { get; private set; }

        private async void ExecuteOnSaveClickedCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            try
            {
                if (isValid())
                {
                    var check = await _contactService.SaveAsync(new Entities.ContactRequest()
                    {
                        Id = Id,
                        Name = Name,
                        Position = Position,
                        Phone = Phone,
                        Age = Age,
                        Gender = Gender,
                        Email = Email,
                        Adress = Adress
                    });

                    if (check == "Success")
                    {
                        await _navigationService.GoBackAsync();
                    }


                }
            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private bool isValid()
        {
            if (string.IsNullOrEmpty(Name)
                || string.IsNullOrEmpty(Position)
                || Age == 0 || Age < 0
                || string.IsNullOrEmpty(Gender)
                || string.IsNullOrEmpty(Email)
                || string.IsNullOrEmpty(Adress))
            {
                Application.Current.MainPage.DisplayAlert("Thông báo", "Thông tin không đầy đủ", "OK");
                return false;
            }
            if (Phone.Length < 10)
            {
                Application.Current.MainPage.DisplayAlert("Thông báo", "Số điện thoại chưa đủ", "OK");
                return false;
            }
            Regex regex1 = new Regex("^[a-zA-Z]+[a-zA-Z0-9]+[[a-zA-Z0-9-_.!#$%'*+/=?^]{1,20}@[a-zA-Z0-9]{1,20}.[a-zA-Z]{2,3}$");
            if (!regex1.IsMatch(Email))
            {
                Application.Current.MainPage.DisplayAlert("Thông báo", "Email không chính xác", "OK");
                return false;
            }
            return true;
        }

        public DelegateCommand OnDeleteClicked { get; private set; }

        private async void ExecuteOnDeleteClickedCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            try
            {
                var answer = await Application.Current.MainPage.DisplayAlert("Thông báo", "Bạn muốn xoá thông tin này", "OK", "CANCEL");
                if (answer)
                {
                    var check = await _contactService.DeleteAsync(new Entities.ContactRequest()
                    {
                        Id = Id
                    });

                    if (check == "Success")
                    {
                        await _navigationService.GoBackAsync();
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public DelegateCommand OnCancelClicked { get; private set; }

        private async void ExecuteOnCancelClickedCommandAsync()
        {
            await _navigationService.GoBackAsync();
        }


        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters != null)
            {
                if (parameters.TryGetValue("_contact", out ContactRespone data))
                {
                    Id = data.Id;
                    Name = data.Name;
                    Position = data.Position;
                    Phone = data.Phone;
                    Age = data.Age;
                    Gender = data.Gender;
                    Email = data.Email;
                    Adress = data.Adress;
                    IsShow = true;
                }
            }
            base.OnNavigatedTo(parameters);
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            base.OnNavigatedFrom(parameters);
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }


}
