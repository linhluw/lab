﻿using BA_Xamarin.Service;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace BA_Xamarin.Core.ViewModels
{
    public class MapViewMVVMViewModel : ViewModelBase
    {
        private readonly IAuthenticationAPIService _authenticationService;

        private CameraUpdate cameraUpdate = null;

        public CameraUpdate CameraUpdate
        {
            get => cameraUpdate;
            set => SetProperty(ref cameraUpdate, value);
        }

        private bool isEnabledAddPinVanChuong = true;

        public bool IsEnabledAddPinVanChuong { get => isEnabledAddPinVanChuong; set => SetProperty(ref isEnabledAddPinVanChuong, value); }

        private bool isEnabledAddPinKimDong = true;
        public bool IsEnabledAddPinKimDong { get => isEnabledAddPinKimDong; set => SetProperty(ref isEnabledAddPinKimDong, value); }

        private bool isEnabledAddGroundOverlay = true;

        public bool IsEnabledAddGroundOverlay { get => isEnabledAddGroundOverlay; set => SetProperty(ref isEnabledAddGroundOverlay, value); }

        private bool isEnabledAddLines = true;
        public bool IsEnabledAddLines { get => isEnabledAddLines; set => SetProperty(ref isEnabledAddLines, value); }

        private Pin pin;

        public Pin Pin
        {
            get => pin;
            set => SetProperty(ref pin, value);
        }

        public ObservableCollection<Pin> Pins { get; set; }

        public MapViewMVVMViewModel(INavigationService navigationService, IAuthenticationAPIService authenticationService) : base(navigationService)
        {
            this._authenticationService = authenticationService;
        }

        /// <summary>Gets the default.</summary>
        /// <Modified>
        /// Name     Date         Comments
        /// linhlv  11/6/2019   created
        /// Load mặc định địa điểm bản đồ
        /// </Modified>
        void GetDefault()
        {
            CameraUpdate =
               CameraUpdateFactory.NewCameraPosition(
                   new CameraPosition(
                       new Position(20.974682, 105.843101), 15
                   ));
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            GetDefault();
            base.OnNavigatedTo(parameters);
        }

        /// <summary>Gets the add pin van chuong command.</summary>
        /// <value>The add pin van chuong command.</value>
        /// <Modified>
        /// Name     Date         Comments
        /// linhlv  11/6/2019   created
        /// Thêm địa chỉ trên bản đồ (Văn Chương)
        /// </Modified>
        public Command AddPinVanChuongCommand => new Command(() =>
        {
            Pin = new Pin()
            {
                Type = PinType.Place,
                Label = "Van Chuong nha Linh",
                Address = "Ngo Van Chuong, Hanoi, Vietnam",
                Position = new Position(21.021501, 105.834192),
                Rotation = 33.3f
            };
            Pins?.Add(Pin);
            CameraUpdate =
                CameraUpdateFactory.NewCameraPosition(
                    new CameraPosition(
                        new Position(21.021501, 105.834192), 15
                    ));
            IsEnabledAddPinVanChuong = false;
        });

        /// <summary>Gets the add pin kim dong command.</summary>
        /// <value>The add pin kim dong command.</value>
        /// <Modified>
        /// Name     Date         Comments
        /// linhlv  11/6/2019   created
        /// Thêm địa chỉ trên bản đồ (Kim Đồng)
        /// </Modified>
        public Command AddPinKimDongCommand => new Command(() =>
        {
            Pin = new Pin()
            {
                Type = PinType.Place,
                Label = "Kim Dong nha Linh",
                Address = "Pho Kim Dong, Hanoi, Vietnam",
                Position = new Position(20.981755, 105.843904),
                Rotation = 33.3f
            };
            Pins?.Add(Pin);
            CameraUpdate =
                CameraUpdateFactory.NewCameraPosition(
                    new CameraPosition(
                        new Position(20.981755, 105.843904), 15
                    ));
            IsEnabledAddPinKimDong = false;
        });

        public ObservableCollection<GroundOverlay> GroundOverlays { get; set; }

        public ObservableCollection<Polygon> Polygons { get; set; }

        /// <summary>Gets the add ground overlay command.</summary>
        /// <value>The add ground overlay command.</value>
        /// <Modified>
        /// Name     Date         Comments
        /// linhlv  11/6/2019   created
        /// Thêm icon ô tô và khung bo ô tô tại địa chỉ Kim Đồng
        /// </Modified>
        public Command AddGroundOverlayCommand => new Command(() =>
        {
            var _overlay = new GroundOverlay()
            {
                Bounds = new Bounds(
                 new Position(20.981755, 105.843804),
                 new Position(20.981855, 105.843904)),
                Icon = BitmapDescriptorFactory.FromBundle("car.png"),
                Bearing = 300,
                ZIndex = 1
            };
            GroundOverlays.Add(_overlay);

            var polygon = new Polygon()
            {
                FillColor = Color.Transparent,
                StrokeColor = Color.Blue,
                StrokeWidth = 2f
            };
            polygon.Positions.Add(new Position(20.981755, 105.843804));
            polygon.Positions.Add(new Position(20.981755, 105.843904));
            polygon.Positions.Add(new Position(20.981855, 105.843904));
            polygon.Positions.Add(new Position(20.981855, 105.843804));
            polygon.Positions.Add(new Position(20.981755, 105.843804));
            Polygons.Add(polygon);

            CameraUpdate =
               CameraUpdateFactory.NewCameraPosition(
                   new CameraPosition(
                       new Position(20.981755, 105.843904), 15
                   ));
            IsEnabledAddGroundOverlay = false;
        });

        public ObservableCollection<Polyline> Polylines { get; set; }

        /// <summary>Gets the add lines command.</summary>
        /// <value>The add lines command.</value>
        /// <Modified>
        /// Name     Date         Comments
        /// Thêm chỉ đường Kim Đồng - Văn Chương
        /// </Modified>
        public Command AddLinesCommand => new Command(() =>
        {
            var polyline = new Polyline();
            polyline.Positions.Add(new Position(20.981794, 105.843821));
            polyline.Positions.Add(new Position(20.982184, 105.843837));
            polyline.Positions.Add(new Position(20.983697, 105.843810));
            polyline.Positions.Add(new Position(20.983737, 105.841423));
            ////////
            polyline.Positions.Add(new Position(20.983993, 105.841101));
            polyline.Positions.Add(new Position(20.995261, 105.841255));
            polyline.Positions.Add(new Position(21.000059, 105.841354));
            polyline.Positions.Add(new Position(21.000324, 105.841402));
            polyline.Positions.Add(new Position(21.003690, 105.841386));
            polyline.Positions.Add(new Position(21.005112, 105.841466));
            polyline.Positions.Add(new Position(21.007791, 105.841428));
            polyline.Positions.Add(new Position(21.009849, 105.838086));
            polyline.Positions.Add(new Position(21.018067, 105.830046));
            ///
            polyline.Positions.Add(new Position(21.018673, 105.830037));
            polyline.Positions.Add(new Position(21.021419, 105.831318));
            polyline.Positions.Add(new Position(21.021183, 105.831814));
            polyline.Positions.Add(new Position(21.021680, 105.832996));
            polyline.Positions.Add(new Position(21.022143, 105.833014));
            polyline.Positions.Add(new Position(21.022291, 105.833242));
            polyline.Positions.Add(new Position(21.022256, 105.833564));
            polyline.Positions.Add(new Position(21.022016, 105.834009));
            polyline.Positions.Add(new Position(21.021505, 105.834084));
            polyline.IsClickable = true;
            polyline.StrokeColor = Color.Red;
            polyline.StrokeWidth = 5f;
            polyline.Tag = "POLYLINE"; // Can set any object
            Polylines.Add(polyline);

            IsEnabledAddLines = false;
        });
    }
}