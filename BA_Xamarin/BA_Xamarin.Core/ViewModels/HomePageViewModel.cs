﻿using System.Collections.ObjectModel;
using BA_Xamarin.Entities;
using BA_Xamarin.Service;
using Prism.Navigation;

namespace BA_Xamarin.Core.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        public HomePageViewModel(INavigationService navigationService, IAuthenticationService authenticationService) : base(navigationService)
        {
            this._authenticationService = authenticationService;

            getListUserLogin();

        }

        private ObservableCollection<UserRespone> listUser;
        public ObservableCollection<UserRespone> ListUser { get => listUser; set => SetProperty(ref listUser, value); }

        private void getListUserLogin()
        {
            var listuser = _authenticationService.All();
            if (listuser != null)
            {
                ListUser = new ObservableCollection<UserRespone>(listuser);
            }

        }


    }
}
