﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using BA_Xamarin.Entities.Constant;
using BA_Xamarin.Service;
using BA_Xamarin.Utilities.Logs;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms;

namespace BA_Xamarin.Core.ViewModels
{
    public class MainLoginViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        public DelegateCommand LoginCommand { get; private set; }
             
        public MainLoginViewModel(INavigationService navigationService, IAuthenticationService authenticationService) : base(navigationService)
        {
            this._authenticationService = authenticationService;
            LoginCommand = new DelegateCommand(() => Login());
        }

        private string username;
        public string UserName { get => username; set => SetProperty(ref username, value); }

        private string password;
        public string Password { get => password; set => SetProperty(ref password, value); }

        private bool isshow;
        public bool IsShow
        {
           
            get => isshow;
            set
            {
                if (SetProperty(ref isshow, value))
                {
                    RaisePropertyChanged(nameof(IsNotShow));
                }
            }
        }

        public bool IsNotShow => !IsShow;

        private async void Login()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            try
            {
                var userInfo = _authenticationService.LoginAsync(new Entities.LoginRequest()
                {
                    UserName = UserName,
                    Password = Password
                });
                if (userInfo != null)
                {
                    //StaticSettings.User = userInfo;

                    //Lưu thông tin đăng nhập xuống localDB

                    var insertOK = _authenticationService.Add(userInfo);

                    await NavigationService.NavigateAsync("NavigationPage/MainList");
                }
                else
                {

                    await Application.Current.MainPage.DisplayAlert("Thông báo", "Thông tin không chính xác", "OK");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            finally
            {
                IsBusy = false;
            }
        }


    }


}
