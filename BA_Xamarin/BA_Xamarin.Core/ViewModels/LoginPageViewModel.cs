﻿using System;
using System.Reflection;
using BA_Xamarin.Entities.Constant;
using BA_Xamarin.Service;
using BA_Xamarin.Utilities.Logs;
using Prism.Commands;
using Prism.Navigation;

namespace BA_Xamarin.Core.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        public DelegateCommand LoginCommand { get; private set; }

        public LoginPageViewModel(INavigationService navigationService, IAuthenticationService authenticationService) : base(navigationService)
        {
            this._authenticationService = authenticationService;
            LoginCommand = new DelegateCommand(() => Login());
        }

        private string username;
        public string UserName { get => username; set => SetProperty(ref username, value); }

        private string password;
        public string Password { get => password; set => SetProperty(ref password, value); }

        private async void Login()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            try
            {
                var userInfo = _authenticationService.LoginAsync(new Entities.LoginRequest()
                {
                    UserName = UserName,
                    Password = Password
                });
                if (userInfo != null)
                {
                    //StaticSettings.User = userInfo;

                    //Lưu thông tin đăng nhập xuống localDB

                    var insertOK = _authenticationService.Add(userInfo);

                    await NavigationService.NavigateAsync("/HomePage");
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError(MethodInfo.GetCurrentMethod().Name, ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
