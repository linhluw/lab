﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.Constant;
using Prism.AppModel;
using Prism.Mvvm;
using Prism.Navigation;

namespace BA_Xamarin.Core.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible, IApplicationLifecycleAware, IDisposable
    {
        protected INavigationService NavigationService { get; private set; }

        private string title;
        public virtual string Title { get => title; set => SetProperty(ref title, value); }

        private bool isBusy = false;
        public bool IsBusy { get => isBusy; set => SetProperty(ref isBusy, value); }

        //private UserRespone userInfo;

        //public UserRespone UserInfo
        //{
        //    get
        //    {
        //        if (StaticSettings.User != null)
        //        {
        //            userInfo = StaticSettings.User;
        //        }
        //        return userInfo;
        //    }
        //    set => SetProperty(ref userInfo, value);
        //}

        protected bool SetProperty<T>(ref T backingStore, T value, [CallerMemberName] string propertyName = null, params string[] relatedProperty)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            RaisePropertyChanged(propertyName);

            foreach (var property in relatedProperty)
            {
                RaisePropertyChanged(property);
            }

            return true;
        }


        public ViewModelBase(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        ~ViewModelBase()
        {
        }

        public virtual void Destroy()
        {
            OnDestroy();

        }

        public virtual void OnDestroy()
        {
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {
        }
        public virtual void OnNavigatingTo(INavigationParameters parameters)
        {
        }

        public virtual void OnPushed()
        {
        }

        public virtual void OnResume()
        {
        }

        public virtual void OnSleep()
        {
        }

        public virtual void Dispose()
        {
        }
    }
}
