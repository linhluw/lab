﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Threading.Tasks;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.Constant;
using BA_Xamarin.Service;
using BA_Xamarin.Utilities.Logs;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms;

namespace BA_Xamarin.Core.ViewModels
{
    public class MainListAPIViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;

        private readonly IContactAPIService _contactService;

        public MainListAPIViewModel(INavigationService navigationService, IContactAPIService contactService) : base(navigationService)
        {
            this._navigationService = navigationService;
            this._contactService = contactService;
            Title = "Contact List";
            OnItemAdded = new DelegateCommand(() => ExecuteOnItemAddedCommand());
            OnItemSelected = new DelegateCommand<ContactRespone>(ExecuteOnItemSelectedCommand);
        }

        private ObservableCollection<ContactRespone> listcontact;

        public ObservableCollection<ContactRespone> ListContact
        {
            get => listcontact;
            set => SetProperty(ref listcontact, value);
        }


        async void GetAll()
        {
            var listcontact = await _contactService.GetAll();
            if (listcontact != null)
            {
                ListContact = new ObservableCollection<ContactRespone>(listcontact);
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            GetAll();
            base.OnNavigatedTo(parameters);
        }
        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            base.OnNavigatedFrom(parameters);
        }
        public override void Destroy()
        {
            base.Destroy();
        }

        public DelegateCommand OnItemAdded { get; private set; }

        async void ExecuteOnItemAddedCommand()
        {
            await _navigationService.NavigateAsync("MainDetailAPI");
        }
        public DelegateCommand<ContactRespone> OnItemSelected { get; private set; }

        async void ExecuteOnItemSelectedCommand(ContactRespone _contact)
        {
            var p = new NavigationParameters
            {
                { "_contact", _contact}
            };
            await _navigationService.NavigateAsync("MainDetailAPI", p);
        }

    }


}
