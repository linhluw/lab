﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace BA_Xamarin.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapView : ContentPage
    {
        public MapView()
        {
            InitializeComponent();

            buttonAddPinVanChuong.Clicked += (sender, e) =>
            {
                Pin pinVanChuong = null;
                pinVanChuong = new Pin()
                {
                    Type = PinType.Place,
                    Label = "Van Chuong nha Linh",
                    Address = "Ngo Van Chuong, Hanoi, Vietnam",
                    Position = new Position(21.021501, 105.834192),
                    Rotation = 33.3f,
                };

                map.Pins.Add(pinVanChuong);
                map.MoveToRegion(MapSpan.FromCenterAndRadius(pinVanChuong.Position, Distance.FromMeters(500)));

                ((Button)sender).IsEnabled = false;
                // buttonRemovePinTokyo.IsEnabled = true;
            };

            buttonAddPinKimDong.Clicked += (sender, e) =>
            {
                Pin pinKimDong = null;
                pinKimDong = new Pin()
                {
                    Type = PinType.Place,
                    Label = "Kim Dong nha Linh",
                    Address = "Pho Kim Dong, Hanoi, Vietnam",
                    Position = new Position(20.981755, 105.843904),
                    Rotation = 33.3f,
                };

                map.Pins.Add(pinKimDong);
                map.MoveToRegion(MapSpan.FromCenterAndRadius(pinKimDong.Position, Distance.FromMeters(500)));

                ((Button)sender).IsEnabled = false;

            };

           
            buttonAddGroundOverlay.Clicked += (sender, e) =>
            {
                GroundOverlay _overlay = null;
                _overlay = new GroundOverlay()
                {
                    Bounds = new Bounds(
                    new Position(20.981755, 105.843804),
                    new Position(20.981855, 105.843904)),
                    Icon = BitmapDescriptorFactory.FromBundle("car.png"),
                    Bearing = 300,
                    ZIndex = 1
                };
                map.GroundOverlays.Add(_overlay);

                
                var polygon = new Polygon()
                {
                    FillColor = Color.Transparent,
                    StrokeColor = Color.Blue,
                    StrokeWidth = 2f
                };
                polygon.Positions.Add(new Position(20.981755, 105.843804));
                polygon.Positions.Add(new Position(20.981755, 105.843904));
                polygon.Positions.Add(new Position(20.981855, 105.843904));
                polygon.Positions.Add(new Position(20.981855, 105.843804));
                polygon.Positions.Add(new Position(20.981755, 105.843804));
                map.Polygons.Add(polygon);

                ((Button)sender).IsEnabled = false;
            };

            buttonAddLines.Clicked += (sender, e) =>
            {
                Polyline polyline = null;
                polyline = new Polyline();
                polyline.Positions.Add(new Position(20.981794, 105.843821));
                polyline.Positions.Add(new Position(20.982184, 105.843837));
                polyline.Positions.Add(new Position(20.983697, 105.843810));
                polyline.Positions.Add(new Position(20.983737, 105.841423));
                ////////
                polyline.Positions.Add(new Position(20.983993, 105.841101));
                polyline.Positions.Add(new Position(20.995261, 105.841255));
                polyline.Positions.Add(new Position(21.000059, 105.841354));
                polyline.Positions.Add(new Position(21.000324, 105.841402));
                polyline.Positions.Add(new Position(21.003690, 105.841386));
                polyline.Positions.Add(new Position(21.005112, 105.841466));
                polyline.Positions.Add(new Position(21.007791, 105.841428));
                polyline.Positions.Add(new Position(21.009849, 105.838086));
                polyline.Positions.Add(new Position(21.018067, 105.830046));
                ///
                polyline.Positions.Add(new Position(21.018673, 105.830037));
                polyline.Positions.Add(new Position(21.021419, 105.831318));
                polyline.Positions.Add(new Position(21.021183, 105.831814));
                polyline.Positions.Add(new Position(21.021680, 105.832996));
                polyline.Positions.Add(new Position(21.022143, 105.833014));
                polyline.Positions.Add(new Position(21.022291, 105.833242));
                polyline.Positions.Add(new Position(21.022256, 105.833564));
                polyline.Positions.Add(new Position(21.022016, 105.834009));
                polyline.Positions.Add(new Position(21.021505, 105.834084));
                polyline.IsClickable = true;
                polyline.StrokeColor = Color.Red;
                polyline.StrokeWidth = 5f;
                polyline.Tag = "POLYLINE"; // Can set any object
                map.Polylines.Add(polyline);

                ((Button)sender).IsEnabled = false;
            
            };
        }
    }
}