﻿using AutoMapper;
using BA_Xamarin.Entities;
using BA_Xamarin.Entities.RealmEntity;

namespace BA_Xamarin.Core
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Reset();

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<UserRealm, UserRespone>();
                cfg.CreateMap<UserRespone, UserRealm>();
                cfg.CreateMap<ContactRealm, ContactRespone>();
                cfg.CreateMap<ContactRespone, ContactRealm>();
            });
        }
    }
}
