﻿using BA_Xamarin.Core.ViewModels;
using BA_Xamarin.Core.Views;
using BA_Xamarin.Entities.Infrastructure.Repository;
using BA_Xamarin.Service;
using Prism.Ioc;
using Xamarin.Forms;

namespace BA_Xamarin.Core.Setup
{
    public static class BAXamarinSetup
    {
        public static void Initialize()
        {
            AutoMapperConfig.Initialize();
        }

        public static void RegisterServices(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton(typeof(IRealmBaseService<,>), typeof(RealmBaseService<,>));

            containerRegistry.Register<IRealmConnection, RealmConnection>();
            containerRegistry.Register<IBaseRepository, BaseRepository>();


            containerRegistry.Register<IRequestProvider, RequestProvider>();
            containerRegistry.Register<IAuthenticationService, AuthenticationService>();
            containerRegistry.Register<IAuthenticationAPIService, AuthenticationAPIService>();
            containerRegistry.Register<IContactService, ContactService>();
            containerRegistry.Register<IContactAPIService, ContactAPIService>();
        }

        public static void RegisterPages(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            ///////////////////////////
            containerRegistry.RegisterForNavigation<MainLogin, MainLoginViewModel>();
            containerRegistry.RegisterForNavigation<MainList, MainListViewModel>();
            containerRegistry.RegisterForNavigation<MainDetail, MainDetailViewModel>();
            ///////////////////////////
            containerRegistry.RegisterForNavigation<MainLoginAPI, MainLoginAPIViewModel>();
            containerRegistry.RegisterForNavigation<MainListAPI, MainListAPIViewModel>();
            containerRegistry.RegisterForNavigation<MainDetailAPI, MainDetailAPIViewModel>();
            ////////////////////////////
            containerRegistry.RegisterForNavigation<MapView>();
            containerRegistry.RegisterForNavigation<MapViewMVVM, MapViewMVVMViewModel>();
        }
    }
}
