﻿using BA_Xamarin.Core.Setup;
using Prism;
using Prism.Ioc;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BA_Xamarin.Core
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer)
        {
            
        }

        protected override void OnInitialized()
        {

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTY2NDA4QDMxMzcyZTMzMmUzME1IaXVHRlp2TG9JemFsTTBSU3Bsbm13RHBMUEliVTM1a1lKbVNtQ2FXa3M9");
            InitializeComponent();

            BAXamarinSetup.Initialize();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            BAXamarinSetup.RegisterServices(containerRegistry);
            BAXamarinSetup.RegisterPages(containerRegistry);

        }
    }
}
