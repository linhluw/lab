﻿using BA_Xamarin.Core;
using Prism;

namespace BA_Xamarin
{
    public class BA_XamarinApp : App
    {
        public BA_XamarinApp(IPlatformInitializer initializer) : base(initializer)
        {
        }

        protected async override void OnInitialized()
        {
            base.OnInitialized();

            await NavigationService.NavigateAsync("/MapViewMVVM");

            //Resources.MergedDictionaries.Add(new Colors());
            //Resources.MergedDictionaries.Add(new Fonts());
            //Resources.MergedDictionaries.Add(new Converters());
            //Resources.MergedDictionaries.Add(new Styles.Styles());
        }
    }
}
